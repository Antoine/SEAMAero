__all__ = ['SEAMAnalyticBEM']

import numpy as np
from openmdao.main.api import Component
from openmdao.lib.datatypes.api import Float, Array, Int

from fusedwind.lib.bezier import BezierCurve
from fusedwind.lib.cubicspline import NaturalCubicSpline
from scipy.interpolate import Akima1DInterpolator

class SEAMAnalyticBEM(Component):
    """

    """

    hub_fraction = Float(0.03, iotype='in', desc='Normalized hub radius')
    blade_length = Float(1., iotype='in', units='m', desc='Blade length')

    design_CT = Float(0.89, iotype='in', desc='Thrust coefficient')
    tsr = Float(7.5, iotype='in', desc='tip speed ratio')
    nB = Int(3, iotype='in', desc='number of blades')

    Cl_d = Array(iotype='in', desc='Design Cl')
    alpha_d = Array(iotype='in', desc='Design angle of attack')
    LD_d = Array(iotype='in', desc='Design L/D')

    CP = Float(iotype='out', desc='rotor power coefficient')
    CT = Float(iotype='out', desc='rotor thrust coefficient')
    Cp = Array(iotype='out', desc='local power coefficient')

    s = Array(iotype='out', desc='chord distribution')
    chord = Array(iotype='out', desc='chord distribution')
    twist = Array(iotype='out', desc='twist distribution')

    def __init__(self, ni=20):
        super(SEAMAnalyticBEM, self).__init__()

        self.ni = ni

        self.Cp = np.zeros(ni)
        self.chord = np.zeros(ni)
        self.twist = np.zeros(ni)

    def execute(self):
        """ do your calculations here """

        rad2deg = 180. / np.pi

        # bz = BezierCurve()
        # bz.add_control_point(np.array([0, 0]))
        # bz.add_control_point(np.array([0.2, self.Cl_droot]))
        # bz.add_control_point(np.array([0.5, self.Cl_dmid]))
        # bz.add_control_point(np.array([1., self.Cl_dtip]))

        # Cl_d = points

        CT = self.design_CT
        F = 0.988
        r = np.linspace(self.hub_fraction, 1, self.ni)
        nB = self.nB
        CT_2d = CT / F
        tsr = self.tsr
        ltsr = r * tsr
        ld = self.LD_d
        Cl_d = self.Cl_d

        alpha_d = self.alpha_d

        a = 0.08921 * (CT_2d / F)**3 + 0.05450 * (CT_2d / F)**2 + 0.25116 * (CT_2d / F)
        A1 = 0.5 * nB * (1 + tsr**2 / (1 - a)**2)**0.5
        A2 = CT_2d / tsr**2
        a_t = 0.5 * (1 + A2 / r**2)**0.5 - 0.5
        CT_F = -1.392 / (1.2 + A1) * CT_2d
        CT = CT_2d + CT_F

        CF_F = -1.4 / (2.3 + A1) * CT_2d
        CF = 2./3. * CT_2d + CF_F
        CP_d = -CF * tsr / ld
        CP_a = (4.906 * A2**2 - 1.173 * A2 - 0.002362) * CT_2d * (1. - a)
        CP = (1. - a) * CT + CP_d + CP_a

        # distributed
        gamma = CT_2d * np.pi / (nB * tsr) * F / (1 + a_t)
        Cp = CT_2d * F * ((1 -a) / (1 + a_t) - ltsr / ld)

        phi = np.arctan((1 - a) / (0.5 * (1 + (1 + A2 / r**2)) * tsr * r))
        twist = -(phi * rad2deg - alpha_d)
        chord = CT_2d * (2. * np.pi * F * r) / \
                    (((1. - a)**2 + tsr**2 * r**2 * (1 + a_t)**2) * np.cos(phi)) / (Cl_d * nB)

        self.Cf = CT_2d * (1./3. * r**3 - r + 2./3.) + CF_F - r * CT_F

        self.CP = np.trapz(Cp, r)
        self.Cp = Cp
        self.CT = CT
        self.s = r
        self.chord = chord
        self.twist = twist

    def plot(self, fig):
        """
        function to generate Bokeh plot for web GUI.

        Also callable from an ipython notebook

        parameters
        ----------
        fig: object
            Bokeh bokeh.plotting.figure object

        returns
        -------
        fig: object
            Bokeh bokeh.plotting.figure object
        """
        try:
            # formatting
            fig.title = 'Blade geometry'
            fig.xaxis[0].axis_label = 'Span fraction [-]'
            fig.yaxis[0].axis_label = 'Chord fraction [-]'

            # fatigue, ultimate and final thickness line plots
            fig.line(self.s, self.chord, line_color='blue',
                                     line_width=2,
                                     legend='Chord')

            fig.legend[0].orientation = 'top_right'
        except:
            pass

        return fig

if __name__ == '__main__':

    d = SEAMAnalyticBEM(20)
    d.LD = np.linspace(50, 140, 20)
    d.design_Cl = np.linspace(2., 1.4, 20)
    d.alpha_d = np.ones(20) * 6.
    d.CT = 0.89
    d.execute()
